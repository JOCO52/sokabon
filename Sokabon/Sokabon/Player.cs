﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokabon
{
    class Player: Tile
    {
        //data
        private Level ourLevel;
        private float timeSinceLastMove = 0;
        private const float MOVE_COOLDOWN = 0.1f; 

        //behaviour
        public Player(Texture2D newTexture, Level newLevel) : base(newTexture)//passes the informaitonup to the parent class(base class. Sprite
        {
            ourLevel = newLevel;
        }

        public override void Update(GameTime gameTime)
        {
            //get curent time elapsed since start of game
            float frmetime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frmetime;

            KeyboardState keyboardState = Keyboard.GetState();

            Vector2 movementInput = Vector2.Zero;

            //check each key
            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
            }

            if (movementInput != Vector2.Zero && timeSinceLastMove >= MOVE_COOLDOWN)
            {
                TryMove(movementInput);
                timeSinceLastMove = 0;
            }
               
        }

        private bool TryMove(Vector2 direction)
        {
            Vector2 newGridPos = GetTilePosition() + direction;

            //ask level what is in thi slot already
            Tile tileInDirection = ourLevel.GetTileAtPos(newGridPos);

            //if the target tile is a wall then we cant move there return false
            if(tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }

            //if the target tile is a box then try to move it
            if (tileInDirection != null && tileInDirection is Box)
            {
                Box targetBox = tileInDirection as Box;
                bool pushSuccess = targetBox.TryPush(direction);

                if (pushSuccess == false)
                {
                    return false;
                }
            }

            //we need to ask the level if we can mvoe to the new position
            //see if the move was succesful
            bool moveResult = ourLevel.TryMoveTile(this, newGridPos);
            //return true or false base on move succesfulness
            return moveResult;
        }
    }
}
