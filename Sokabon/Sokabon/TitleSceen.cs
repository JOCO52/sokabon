﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokabon
{
    class TitleSceen : Screen
    {
        //Data
        private Text gameName;
        private Text startPrompt;
        private Game1 game;


        //Behaviour

            public TitleSceen(Game1 newGame)
            {
               game = newGame;
            }
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largefont");
            SpriteFont mainFont = content.Load<SpriteFont>("fonts/mainfont");

            gameName = new Text(titleFont);
            gameName.SetText("SOKABON");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.Red);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            startPrompt = new Text(mainFont);
            startPrompt.SetText("Press ENTER to START");
            startPrompt.SetAlignment(Text.Alignment.CENTRE);
            startPrompt.SetColor(Color.Magenta);
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 200));
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            gameName.draw(spriteBatch);
            startPrompt.draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            //check if player has pressed enter
            KeyboardState keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                game.CHangeScreen("level");
            }

        }

    }
}
