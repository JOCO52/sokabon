﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokabon
{
    class Tile : Sprite
    {
        //----------------------
        //data
        //----------------------
        private Vector2 tilePosition;

        private const int TILE_SIZE = 100;

        //----------------------
        //behaviour
        //-----------------------
        public Tile(Texture2D newTexture) : base(newTexture)//passes the informaitonup to the parent class(base class. Sprite
        {

        }

        public void SetTilePosition(Vector2 newTilePosition)
        {
            tilePosition = newTilePosition;
            //set our position based on tile position
            //multiply our tile position by the tile size
            SetPosition(tilePosition * TILE_SIZE);
        }

        public virtual void Update(GameTime gameTime)
        {
            //Blank -to be impleneted by derived or child classes
        }

        public Vector2 GetTilePosition()
        {
            return tilePosition;
        }
    }
}
