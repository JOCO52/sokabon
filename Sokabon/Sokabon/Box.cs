﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokabon
{
    class Box : Tile
    {
        private Level ourLevel;
        private bool onGoal = false;
        private Texture2D onGoalText;
        private Texture2D offGoalText;
        public Box(Texture2D newOffGoalTexture, Texture2D newOnGoalTexture, Level newLevel) : base(newOffGoalTexture)//passes the informaitonup to the parent class(base class. Sprite
        {
            ourLevel = newLevel;
            offGoalText = newOffGoalTexture;
            onGoalText = newOnGoalTexture;
        }

        public bool TryPush(Vector2 direction)
        {
            //new position the box will be in after the push
            Vector2 newGridPos = GetTilePosition() + direction;
            //ask level what is in thi slot already
            Tile tileInDirection = ourLevel.GetTileAtPos(newGridPos);

            //if the target tile is a wall then we cant move there return false
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Box)
            {
                return false;
            }

            //ask waht is on floor in this direction
            Tile FloorInDirection = ourLevel.GetFloorAtPos(newGridPos);

            //if the target tile is a goal then we can move
            if (FloorInDirection != null && FloorInDirection is Goal)
            {
                EnterGoal();
            }
            else if (onGoal == true)
            {
                ExitGoal();
            }


            return ourLevel.TryMoveTile(this, newGridPos);

            
        }

        private void EnterGoal()
        {
            onGoal = true;
            texture = onGoalText;
            ourLevel.EvaluateWinConditions();
        }

        private void ExitGoal()
        {
            onGoal = false;
            texture = offGoalText;
        }

        public bool GetOnGoal()
        {
            return onGoal;
        }




    }
}
