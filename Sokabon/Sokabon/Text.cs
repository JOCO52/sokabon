﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokabon
{
    class Text
    {
        //----------------------
        //ENUMS
        //----------------------
        public enum Alignment
        {
            TOP_LEFT,
            TOP,
            TOP_RIGHT,
            LEFT,
            CENTRE,
            RIGHT,
            BOTT_LEFT,
            BOTTOM,
            BOTTOM_RIGHT

        }

        //----------------------
        //Data
        //----------------------
        SpriteFont font;
        string textstring;
        Vector2 position;
        Color color= Color.White;
        Alignment alignment;
        


        //----------------------
        //behaviour
        //----------------------

        public Text(SpriteFont newFont)
        {
            font = newFont;
        }

        public void draw(SpriteBatch spriteBatch)
        {
            Vector2 adjustedPosition = position;
            Vector2 textSize = font.MeasureString(textstring);

            switch (alignment)
            {
                case Alignment.TOP_LEFT:
                    //do nothign alligned this way by default
                    break;
                case Alignment.TOP:
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case Alignment.TOP_RIGHT:
                    adjustedPosition.X -= textSize.X;
                    break;
                case Alignment.LEFT:
                    adjustedPosition.Y -= textSize.Y / 2;
                    break;
                case Alignment.CENTRE:
                    adjustedPosition.Y -= textSize.Y / 2;
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case Alignment.RIGHT:
                    adjustedPosition.Y -= textSize.Y / 2;
                    adjustedPosition.X -= textSize.X;
                    break;
                case Alignment.BOTT_LEFT:
                    adjustedPosition.Y -= textSize.Y;
                    break;
                case Alignment.BOTTOM:
                    adjustedPosition.Y -= textSize.Y;
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case Alignment.BOTTOM_RIGHT:
                    adjustedPosition.Y -= textSize.Y;
                    adjustedPosition.X -= textSize.X;
                    break;
            }


            spriteBatch.DrawString(font, textstring, adjustedPosition, color);
        }

        public void SetText(String text)
        {
            textstring = text;
        }
 


        public void SetPosition(Vector2 newPos)
        {
            position = newPos;
        }

        public void SetAlignment(Alignment newAlign)
        {
            alignment = newAlign;
        }

        public void SetColor(Color newColor)
        {
            color = newColor;
        }
    }
}   
