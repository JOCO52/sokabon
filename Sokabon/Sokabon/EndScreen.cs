﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokabon
{
    class EndScreen : Screen
    {
        //Data
        private Text gameName;
        private Text startPrompt;
        private Game1 game;

        //Behaviour
        public EndScreen(Game1 newGame)

        {
            game = newGame;
        }
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largefont");
            SpriteFont mainFont = content.Load<SpriteFont>("fonts/mainfont");

            gameName = new Text(titleFont);
            gameName.SetText("Game End");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.Red);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            gameName.draw(spriteBatch);
          
        }

        
    }
}
