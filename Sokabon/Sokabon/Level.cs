﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;

namespace Sokabon
{
    class Level : Screen
    {

        //--------------
        //data
        //---------------

        private Tile[,] tiles;
        private int currentLevel;
        private Tile[,] floor;
        private bool loadNExtLEvel = false;
        private Game1 game;


        //assets
        Texture2D wallSprite;
        Texture2D boxSprite;
        Texture2D playerSprite;
        Texture2D goalSprite;
        Texture2D floorSprite;
        Texture2D boxOGSprite;


        //-----------------
        //behaviour
        //-----------------
        public Level(Game1 newGame)
        {
            game = newGame;
        }
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {

            wallSprite = content.Load<Texture2D>("graphics/Wall");
            playerSprite = content.Load<Texture2D>("graphics/PlayerStatic");
            boxSprite = content.Load<Texture2D>("graphics/Box");
            goalSprite = content.Load<Texture2D>("graphics/Goal");
            floorSprite = content.Load<Texture2D>("graphics/Floor");
            boxOGSprite = content.Load<Texture2D>("graphics/BoxStored");

            LoadLevel(1);
        }

        

        public void LoadLevel(int LevelNum)
        {
            currentLevel = LevelNum;
            string baselevelName = "Level_";
            LoadLevel(baselevelName + LevelNum.ToString() + ".txt");
        }

        public void LoadLevel(string filename)
        {
            ClearLevel();
            //create filestream toopne file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(filename);

            //before we read in the individula tiles of our level
            //we need to know how big the level is overall to 
            //create arrays to hold the data

            int lineWidth = 0;//eventually will be level width
            int numLines = 0;
            List<String> lines = new List<string>();
            StreamReader reader = new StreamReader(fileStream);
            string line = reader.ReadLine();
            lineWidth = line.Length;
            while (line != null)
            {
                lines.Add(line);
                if (line.Length != lineWidth)
                {
                    throw new Exception("lines are different widths - error occured on line" + lines.Count);
                }

                //red the next line to ge ready for the next loop
                line = reader.ReadLine();

            }

            //we have read all the lines into the lines list
            //we can now know how many lines there were
            numLines = lines.Count;

            //now we can set up our tile array
            
            tiles = new Tile[lineWidth, numLines];
            floor = new Tile[lineWidth, numLines];


            //loop over every tile position and check every letter there and load base don tile position
            for (int y = 0; y < numLines; ++y)
            {
                for (int x = 0; x < lineWidth; ++x)
                {
                    //load each tile
                    char tiletype = lines[y][x];
                    //TODO;Load the tile
                    LoadTile(tiletype, x, y);

                }
            }

            
        }
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                

                case 'W':
                    CreateWall(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;


                case '.':
                    CreateFloor(tileX, tileY);
                    break;

                case 'P':
                    CreatePlayer(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                case 'B':
                    CreateBox(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                case 'G':
                    CreateGoal(tileX, tileY);
                    break;

                default:
                    break;
            }
        }
        private void ClearLevel()
        {
           //TODO
        }
        private void CreateWall(int tileX, int tileY)
        {
            Wall wall = new Wall(wallSprite);
            Vector2 tilePosition = new Vector2(tileX , tileY);
            wall.SetTilePosition(tilePosition);
            tiles[tileX, tileY] = wall;
        }

        private void CreatePlayer(int tileX, int tileY)
        {
            Player player = new Player(playerSprite, this);
            Vector2 tilePosition = new Vector2(tileX, tileY);
            player.SetTilePosition(tilePosition);
            tiles[tileX, tileY] = player;
        }

        private void CreateBox(int tileX, int tileY)
        {
            Box box = new Box(boxSprite, boxOGSprite, this );
            Vector2 tilePosition = new Vector2(tileX, tileY);
            box.SetTilePosition(tilePosition);
            tiles[tileX, tileY] = box;
        }

        private void CreateGoal(int tileX, int tileY)
        {
            Goal goal = new Goal(goalSprite);
            Vector2 tilePosition = new Vector2(tileX, tileY);
            goal.SetTilePosition(tilePosition);
            floor[tileX, tileY] = goal;
        }

        private void CreateFloor(int tileX, int tileY)
        {
            Floor ground = new Floor(floorSprite);
            Vector2 tilePosition = new Vector2(tileX, tileY);
            ground.SetTilePosition(tilePosition);
            floor[tileX, tileY] = ground;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            foreach (Tile floor in floor)
            {
                if (floor != null)
                    floor.Draw(spriteBatch);

            }

            foreach (Tile tile in tiles )
            {
               if (tile != null)
                    tile.Draw(spriteBatch);

            }
        }
        public override void Update(GameTime gameTime)
        {
            foreach (Tile floor in floor)
            {
                if (floor != null)
                    floor.Update(gameTime);

            }


            foreach (Tile tile in tiles)
            {
                if (tile != null)
                    tile.Update(gameTime);

            }

            if (loadNExtLEvel == true )
            {
                if (currentLevel == 3)
                {
                    EndScreen();
                }
                else
                LoadLevel(currentLevel + 1);
                loadNExtLEvel = false;
            }

           
        }

        public bool TryMoveTile(Tile toMove, Vector2 newPosition)
        {
            //get current tile position
            Vector2 currentTilePosition = toMove.GetTilePosition();

            //check if the new position is within bounds
            int newPosX = (int)newPosition.X;
            int newPosY = (int)newPosition.Y;
            if (newPosX >= 0
                && newPosY >= 0 
                && newPosX < tiles.GetLength(0) //gets length in the x direction
                && newPosY < tiles.GetLength(1))// gets length in the y direction
            {
                //our new position is legal
                //move it
                toMove.SetTilePosition(newPosition);

                //move it from old arary position to new one
                tiles[newPosX, newPosY] = toMove;

                //erase old tile position
                tiles[(int)currentTilePosition.X, (int)currentTilePosition.Y] = null;

                //we did move it so return true
                return true;
            }
            else
            {
                //new position is out of bounds so return false
                return false;
            }
        }

        public Tile GetTileAtPos(Vector2 tilePos)
        {
            //check if the new position is within bounds
            int PosX = (int)tilePos.X;
            int PosY = (int)tilePos.Y;
            if (PosX >= 0
                && PosY >= 0
                && PosX < tiles.GetLength(0) //gets length in the x direction
                && PosY < tiles.GetLength(1))// gets length in the y direction
            {
                //yes this coordinate is legal
                return tiles[PosX, PosY];
            }
            else
            {
                //no this coordinate is not legal
                return null;
            }
            
        }

        public Tile GetFloorAtPos(Vector2 tilePos)
        {
            //check if the new position is within bounds
            int PosX = (int)tilePos.X;
            int PosY = (int)tilePos.Y;
            if (PosX >= 0
                && PosY >= 0
                && PosX < floor.GetLength(0) //gets length in the x direction
                && PosY < floor.GetLength(1))// gets length in the y direction
            {
                //yes this coordinate is legal
                return floor[PosX, PosY];
            }
            else
            {
                //no this coordinate is not legal
                return null;
            }

        }

        public void EvaluateWinConditions()
        {
            foreach (Tile tile in tiles)
            {
                if (tile != null && tile is Box)
                {
                    if ((tile as Box).GetOnGoal() == false)
                    {
                        //at least one box is not on go so we have not won
                        //exit early from check
                        return;
                    }
                }

            }

            //if we got here we know that no tiles are NOT on Goals
            //Therefore all boxe are on goals
            //Therefore we win and should go to the next level
            //TODO Check if were on last level
            loadNExtLEvel = true;
            

        }
        public void EndScreen()
        {
            game.CHangeScreen("end");
        }

    }
}
