﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sokabon
{
    class Sprite
    {
        //--------------
        //Data
        //--------------
        protected Vector2 position;
        protected Texture2D texture;
        protected bool visible = true;

        //---------------
        //behaviour
        //---------------

        public Sprite(Texture2D newtexture)
        {
            texture = newtexture;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(texture, position, Color.White);
            }
        }

        public bool GetVisible()
        {
            return visible;
        }

        public void SetVisible(bool newvisible)
        {
            visible = newvisible;
        }

        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }

        public virtual Rectangle GetBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)texture.Width, (int)texture.Height);
        }

        public void DrawBounds(SpriteBatch spritebatch, GraphicsDevice Graphics)
        {
            Rectangle bounds = GetBound();

            Texture2D boundsTextutre = new Texture2D(Graphics, bounds.Width, bounds.Height);

            Color[] colorData = new Color[bounds.Width * bounds.Height];
            for (int i = 0; i < colorData.Length; i++)
            {
                colorData[i] = Color.White;
            }
            boundsTextutre.SetData(colorData);

            spritebatch.Draw(boundsTextutre, position, Color.White);
        }
    }
}


